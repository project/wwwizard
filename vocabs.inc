<?php
$vid = install_taxonomy_add_vocabulary('Group type', array (
  'group' => 'group',
), array (
  'description' => '',
  'help' => '',
  'relations' => '1',
  'hierarchy' => '1',
  'multiple' => '1',
  'required' => '0',
  'tags' => '0',
  'module' => 'taxonomy',
  'weight' => '0',
));
$newtid1 = install_taxonomy_add_term($vid, 'Fellowship', '', array (
  'name' => 'Spiritual practice',
  'description' => '',
  'weight' => '0',
  'depth' => 0,
  'parents' => 
  array (
    0 => 0,
  ),
));
$newtid3 = install_taxonomy_add_term($vid, 'Social justice', '', array (
  'name' => 'Social justice',
  'description' => '',
  'weight' => '1',
  'depth' => 0,
  'parents' => 
  array (
    0 => 0,
  ),
));
$newtid2 = install_taxonomy_add_term($vid, 'Arts and ideas', '', array (
  'name' => 'Arts and ideas',
  'description' => '',
  'weight' => '4',
  'depth' => 0,
  'parents' => 
  array (
    0 => 0,
  ),
));
$newtid4 = install_taxonomy_add_term($vid, 'Discussion and affinity', '', array (
  'name' => 'Discussion and affinity',
  'description' => '',
  'weight' => '5',
  'depth' => 0,
  'parents' => 
  array (
    0 => 0,
  ),
));
$term = taxonomy_get_term($newtid5);
$term = (array) $term;
$term['parent'] = array($newtid3);
taxonomy_save_term($term);
$term = taxonomy_get_term($newtid6);
$term = (array) $term;
$term['parent'] = array($newtid3);
taxonomy_save_term($term);
