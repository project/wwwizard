<?php
variable_set('adminrole_adminrole', '3');
variable_set('admin_links_delete', 1);
variable_set('admin_links_edit', 1);
variable_set('admin_links_universaledit', 1);
variable_set('admin_menu_rebuild_links', true);
variable_set('anonymous', 'Anonymous');
variable_set('ant_blog', '0');
variable_set('ant_committee', '0');
variable_set('ant_event', '0');
variable_set('ant_group', '0');
variable_set('ant_link', '0');
variable_set('ant_news', '0');
variable_set('ant_newsletter', '2');
variable_set('ant_page', '0');
variable_set('ant_pattern_blog', '');
variable_set('ant_pattern_committee', '');
variable_set('ant_pattern_event', '');
variable_set('ant_pattern_group', '');
variable_set('ant_pattern_link', '');
variable_set('ant_pattern_news', '');
variable_set('ant_pattern_newsletter', 'Newsletter for [field_pubdate-month] [field_pubdate-d], [field_pubdate-yyyy]');
variable_set('ant_pattern_page', '');
variable_set('ant_pattern_profile', '[field_firstname-raw] [field_lastname-raw]');
variable_set('ant_pattern_reclass', '');
variable_set('ant_pattern_sermon', '');
variable_set('ant_pattern_service', 'Service for [field_datetime-month] [field_datetime-d], [field_datetime-yyyy]');
variable_set('ant_pattern_webform', '');
variable_set('ant_php_blog', 0);
variable_set('ant_php_committee', 0);
variable_set('ant_php_event', 0);
variable_set('ant_php_group', 0);
variable_set('ant_php_link', 0);
variable_set('ant_php_news', 0);
variable_set('ant_php_newsletter', 0);
variable_set('ant_php_page', 0);
variable_set('ant_php_profile', 0);
variable_set('ant_php_reclass', 0);
variable_set('ant_php_sermon', 0);
variable_set('ant_php_service', 0);
variable_set('ant_php_webform', 0);
variable_set('ant_profile', '1');
variable_set('ant_reclass', '0');
variable_set('ant_sermon', '0');
variable_set('ant_service', '2');
variable_set('ant_webform', '0');
variable_set('comment_anonymous_committee', 0);
variable_set('comment_anonymous_event', 0);
variable_set('comment_anonymous_group', 0);
variable_set('comment_anonymous_link', 0);
variable_set('comment_anonymous_news', 0);
variable_set('comment_anonymous_newsletter', 0);
variable_set('comment_anonymous_page', 0);
variable_set('comment_anonymous_profile', 0);
variable_set('comment_anonymous_reclass', 0);
variable_set('comment_anonymous_sermon', 0);
variable_set('comment_anonymous_service', 0);
variable_set('comment_anonymous_webform', 0);
variable_set('comment_committee', '0');
variable_set('comment_controls_committee', '3');
variable_set('comment_controls_event', '3');
variable_set('comment_controls_group', '3');
variable_set('comment_controls_link', '3');
variable_set('comment_controls_news', '3');
variable_set('comment_controls_newsletter', '3');
variable_set('comment_controls_page', '3');
variable_set('comment_controls_profile', '3');
variable_set('comment_controls_reclass', '3');
variable_set('comment_controls_sermon', '3');
variable_set('comment_controls_service', '3');
variable_set('comment_controls_webform', '3');
variable_set('comment_default_mode_committee', '4');
variable_set('comment_default_mode_event', '4');
variable_set('comment_default_mode_group', '4');
variable_set('comment_default_mode_link', '4');
variable_set('comment_default_mode_news', '4');
variable_set('comment_default_mode_newsletter', '4');
variable_set('comment_default_mode_page', '4');
variable_set('comment_default_mode_profile', '4');
variable_set('comment_default_mode_reclass', '4');
variable_set('comment_default_mode_sermon', '4');
variable_set('comment_default_mode_service', '4');
variable_set('comment_default_mode_webform', '4');
variable_set('comment_default_order_committee', '1');
variable_set('comment_default_order_event', '1');
variable_set('comment_default_order_group', '1');
variable_set('comment_default_order_link', '1');
variable_set('comment_default_order_news', '1');
variable_set('comment_default_order_newsletter', '1');
variable_set('comment_default_order_page', '1');
variable_set('comment_default_order_profile', '1');
variable_set('comment_default_order_reclass', '1');
variable_set('comment_default_order_sermon', '1');
variable_set('comment_default_order_service', '1');
variable_set('comment_default_order_webform', '1');
variable_set('comment_default_per_page_committee', '50');
variable_set('comment_default_per_page_event', '50');
variable_set('comment_default_per_page_group', '50');
variable_set('comment_default_per_page_link', '50');
variable_set('comment_default_per_page_news', '50');
variable_set('comment_default_per_page_newsletter', '50');
variable_set('comment_default_per_page_page', '50');
variable_set('comment_default_per_page_profile', '50');
variable_set('comment_default_per_page_reclass', '50');
variable_set('comment_default_per_page_sermon', '50');
variable_set('comment_default_per_page_service', '50');
variable_set('comment_default_per_page_webform', '50');
variable_set('comment_event', '0');
variable_set('comment_form_location_committee', '0');
variable_set('comment_form_location_event', '0');
variable_set('comment_form_location_group', '0');
variable_set('comment_form_location_link', '0');
variable_set('comment_form_location_news', '0');
variable_set('comment_form_location_newsletter', '0');
variable_set('comment_form_location_page', '0');
variable_set('comment_form_location_profile', '0');
variable_set('comment_form_location_reclass', '0');
variable_set('comment_form_location_sermon', '0');
variable_set('comment_form_location_service', '0');
variable_set('comment_form_location_webform', '0');
variable_set('comment_group', '0');
variable_set('comment_link', '0');
variable_set('comment_news', '0');
variable_set('comment_newsletter', '0');
variable_set('comment_page', '0');
variable_set('comment_preview_committee', '1');
variable_set('comment_preview_event', '1');
variable_set('comment_preview_group', '1');
variable_set('comment_preview_link', '1');
variable_set('comment_preview_news', '1');
variable_set('comment_preview_newsletter', '1');
variable_set('comment_preview_page', '1');
variable_set('comment_preview_profile', '1');
variable_set('comment_preview_reclass', '1');
variable_set('comment_preview_sermon', '1');
variable_set('comment_preview_service', '1');
variable_set('comment_preview_webform', '1');
variable_set('comment_profile', '0');
variable_set('comment_reclass', '0');
variable_set('comment_sermon', '0');
variable_set('comment_service', '0');
variable_set('comment_subject_field_committee', '1');
variable_set('comment_subject_field_event', '1');
variable_set('comment_subject_field_group', '1');
variable_set('comment_subject_field_link', '1');
variable_set('comment_subject_field_news', '1');
variable_set('comment_subject_field_newsletter', '1');
variable_set('comment_subject_field_page', '1');
variable_set('comment_subject_field_profile', '1');
variable_set('comment_subject_field_reclass', '1');
variable_set('comment_subject_field_sermon', '1');
variable_set('comment_subject_field_service', '1');
variable_set('comment_subject_field_webform', '1');
variable_set('comment_webform', '0');
variable_set('configurable_timezones', '0');
variable_set('content_extra_weights_event', array (
  'title' => '-5',
  'body_field' => '-2',
  'menu' => '-4',
));
variable_set('content_extra_weights_link', array (
  'title' => '-5',
  'body_field' => '-2',
  'menu' => '-3',
));
variable_set('content_extra_weights_newsletter', array (
  'title' => '-5',
  'body_field' => '-1',
  'menu' => '-2',
));
variable_set('content_extra_weights_profile', array (
  'title' => '-5',
  'body_field' => '1',
  'menu' => '-4',
  'taxonomy' => '-3',
));
variable_set('content_extra_weights_reclass', array (
  'title' => '-5',
  'body_field' => '-2',
  'menu' => '-3',
));
variable_set('content_extra_weights_sermon', array (
  'title' => '-5',
  'body_field' => '0',
  'menu' => '-1',
));
variable_set('content_extra_weights_service', array (
  'title' => '-5',
  'body_field' => '-2',
  'menu' => '-3',
));
variable_set('content_profile_profile', array (
  'weight' => '0',
  'user_display' => 'full',
  'edit_link' => 0,
  'edit_tab' => 'sub',
  'add_link' => 1,
  'registration_use' => 1,
  'registration_hide' => 
  array (
    'field_proftitle' => 0,
    'field_firstname' => 0,
    'field_lastname' => 0,
  ),
));
variable_set('content_profile_use_blog', 0);
variable_set('content_profile_use_committee', 0);
variable_set('content_profile_use_event', 0);
variable_set('content_profile_use_group', 0);
variable_set('content_profile_use_link', 0);
variable_set('content_profile_use_news', 0);
variable_set('content_profile_use_newsletter', 0);
variable_set('content_profile_use_page', 0);
variable_set('content_profile_use_profile', 1);
variable_set('content_profile_use_reclass', 0);
variable_set('content_profile_use_sermon', 0);
variable_set('content_profile_use_service', 0);
variable_set('content_profile_use_webform', 0);
variable_set('date_first_day', '0');
variable_set('date_format_long', 'l, F j, Y - H:i');
variable_set('date_format_long_custom', 'l, F j, Y - H:i');
variable_set('date_format_medium', 'D, m/d/Y - H:i');
variable_set('date_format_medium_custom', 'D, m/d/Y - H:i');
variable_set('date_format_short', 'm/d/Y - H:i');
variable_set('date_format_short_custom', 'm/d/Y - H:i');
variable_set('demo_dump_cron', 'uuwebsite');
variable_set('demo_dump_path', 'sites/default/files/demo');
variable_set('demo_reset_interval', '3600');
variable_set('drupal_http_request_fails', false);
variable_set('filter_default_format', '2');
variable_set('filter_html_1', 1);
variable_set('globalredirect_case_sensitive_urls', '1');
variable_set('globalredirect_deslash', '1');
variable_set('globalredirect_menu_check', '-1');
variable_set('globalredirect_nonclean2clean', '1');
variable_set('globalredirect_trailingzero', '-1');
variable_set('image_toolkit', 'gd');
variable_set('imce_profiles', array (
  1 => 
  array (
    'name' => 'User-1',
    'filesize' => '0',
    'quota' => '0',
    'tuquota' => '0',
    'extensions' => '*',
    'dimensions' => '1200x1200',
    'filenum' => '0',
    'directories' => 
    array (
      0 => 
      array (
        'name' => 'images',
        'subnav' => 1,
        'browse' => 1,
        'upload' => 1,
        'thumb' => 1,
        'delete' => 1,
        'resize' => 1,
      ),
    ),
    'thumbnails' => 
    array (
      0 => 
      array (
        'name' => 'Small',
        'dimensions' => '90x90',
        'prefix' => 'small_',
        'suffix' => '',
      ),
      1 => 
      array (
        'name' => 'Medium',
        'dimensions' => '120x120',
        'prefix' => 'medium_',
        'suffix' => '',
      ),
      2 => 
      array (
        'name' => 'Large',
        'dimensions' => '180x180',
        'prefix' => 'large_',
        'suffix' => '',
      ),
    ),
  ),
  2 => 
  array (
    'name' => 'default',
    'filesize' => '0',
    'quota' => '0',
    'tuquota' => '0',
    'extensions' => 'gif png jpg jpeg',
    'dimensions' => '800x600',
    'filenum' => '1',
    'directories' => 
    array (
      0 => 
      array (
        'name' => 'images',
        'subnav' => 0,
        'browse' => 1,
        'upload' => 1,
        'thumb' => 1,
        'delete' => 1,
        'resize' => 1,
      ),
    ),
    'thumbnails' => 
    array (
      0 => 
      array (
        'name' => 'Thumb',
        'dimensions' => '90x90',
        'prefix' => 'thumb_',
        'suffix' => '',
      ),
    ),
  ),
));
variable_set('imce_settings_absurls', 0);
variable_set('imce_settings_replace', '0');
variable_set('imce_settings_textarea', '');
variable_set('menu_blog', array (
));
variable_set('menu_committee', array (
));
variable_set('menu_default_node_menu', 'menu-sitemap');
variable_set('menu_event', array (
));
variable_set('menu_expanded', array (
));
variable_set('menu_group', array (
));
variable_set('menu_link', array (
));
variable_set('menu_masks', array (
  0 => 127,
  1 => 125,
  2 => 63,
  3 => 62,
  4 => 61,
  5 => 59,
  6 => 58,
  7 => 56,
  8 => 45,
  9 => 31,
  10 => 30,
  11 => 29,
  12 => 24,
  13 => 22,
  14 => 21,
  15 => 15,
  16 => 14,
  17 => 11,
  18 => 10,
  19 => 7,
  20 => 6,
  21 => 5,
  22 => 4,
  23 => 3,
  24 => 2,
  25 => 1,
));
variable_set('menu_news', array (
));
variable_set('menu_newsletter', array (
));
variable_set('menu_page', array (
));
variable_set('menu_primary_links_source', '');
variable_set('menu_profile', array (
));
variable_set('menu_reclass', array (
));
variable_set('menu_secondary_links_source', '');
variable_set('menu_sermon', array (
));
variable_set('menu_service', array (
));
variable_set('menu_webform', array (
  0 => 1,
));
variable_set('nice_menus_custom_css', '');
variable_set('nice_menus_menu_1', 'menu-sitemap:0');
variable_set('nice_menus_name_1', 'Nice Menu 1');
variable_set('nice_menus_type_1', 'right');
variable_set('node_options_blog', array (
  0 => 'status',
));
variable_set('node_options_committee', array (
  0 => 'status',
));
variable_set('node_options_event', array (
  0 => 'status',
));
variable_set('node_options_forum', array (
  0 => 'status',
));
variable_set('node_options_group', array (
  0 => 'status',
));
variable_set('node_options_link', array (
  0 => 'status',
));
variable_set('node_options_news', array (
  0 => 'status',
));
variable_set('node_options_newsletter', array (
  0 => 'status',
));
variable_set('node_options_page', array (
  0 => 'status',
));
variable_set('node_options_profile', array (
  0 => 'status',
));
variable_set('node_options_reclass', array (
  0 => 'status',
));
variable_set('node_options_sermon', array (
  0 => 'status',
));
variable_set('node_options_service', array (
  0 => 'status',
));
variable_set('node_options_webform', array (
  0 => 'status',
));
variable_set('pathauto_case', '1');
variable_set('pathauto_ignore_words', 'a,an,as,at,before,but,by,for,from,is,in,into,like,of,off,on,onto,per,since,than,the,this,that,to,up,via,with');
variable_set('pathauto_indexaliases', false);
variable_set('pathauto_indexaliases_bulkupdate', false);
variable_set('pathauto_max_bulk_update', '50');
variable_set('pathauto_max_component_length', '100');
variable_set('pathauto_max_length', '100');
variable_set('pathauto_modulelist', array (
  0 => 'node',
  1 => 'taxonomy',
  2 => 'user',
));
variable_set('pathauto_node_applytofeeds', 'feed');
variable_set('pathauto_node_bulkupdate', 0);
variable_set('pathauto_node_committee_pattern', '');
variable_set('pathauto_node_event_pattern', '');
variable_set('pathauto_node_forum_pattern', '');
variable_set('pathauto_node_group_pattern', '');
variable_set('pathauto_node_image_pattern', '');
variable_set('pathauto_node_link_pattern', '');
variable_set('pathauto_node_newsletter_pattern', '');
variable_set('pathauto_node_news_pattern', '');
variable_set('pathauto_node_page_pattern', '');
variable_set('pathauto_node_pattern', 'content/[title-raw]');
variable_set('pathauto_node_profile_pattern', '');
variable_set('pathauto_node_service_pattern', '');
variable_set('pathauto_node_story_pattern', '');
variable_set('pathauto_node_supportsfeeds', 'feed');
variable_set('pathauto_node_webform_pattern', '');
variable_set('pathauto_punctuation_ampersand', '0');
variable_set('pathauto_punctuation_asterisk', '0');
variable_set('pathauto_punctuation_at', '0');
variable_set('pathauto_punctuation_backtick', '0');
variable_set('pathauto_punctuation_back_slash', '0');
variable_set('pathauto_punctuation_caret', '0');
variable_set('pathauto_punctuation_colon', '0');
variable_set('pathauto_punctuation_comma', '0');
variable_set('pathauto_punctuation_dollar', '0');
variable_set('pathauto_punctuation_double_quotes', '0');
variable_set('pathauto_punctuation_equal', '0');
variable_set('pathauto_punctuation_exclamation', '0');
variable_set('pathauto_punctuation_greater_than', '0');
variable_set('pathauto_punctuation_hash', '0');
variable_set('pathauto_punctuation_hyphen', '1');
variable_set('pathauto_punctuation_left_curly', '0');
variable_set('pathauto_punctuation_left_parenthesis', '0');
variable_set('pathauto_punctuation_left_square', '0');
variable_set('pathauto_punctuation_less_than', '0');
variable_set('pathauto_punctuation_percent', '0');
variable_set('pathauto_punctuation_period', '0');
variable_set('pathauto_punctuation_pipe', '0');
variable_set('pathauto_punctuation_plus', '0');
variable_set('pathauto_punctuation_question_mark', '0');
variable_set('pathauto_punctuation_quotes', '0');
variable_set('pathauto_punctuation_right_curly', '0');
variable_set('pathauto_punctuation_right_parenthesis', '0');
variable_set('pathauto_punctuation_right_square', '0');
variable_set('pathauto_punctuation_semicolon', '0');
variable_set('pathauto_punctuation_tilde', '0');
variable_set('pathauto_punctuation_underscore', '0');
variable_set('pathauto_reduce_ascii', 1);
variable_set('pathauto_separator', '-');
variable_set('pathauto_taxonomy_1_pattern', '');
variable_set('pathauto_taxonomy_2_pattern', '');
variable_set('pathauto_taxonomy_applytofeeds', '');
variable_set('pathauto_taxonomy_bulkupdate', 0);
variable_set('pathauto_taxonomy_pattern', 'category/[vocab-raw]/[catpath-raw]');
variable_set('pathauto_taxonomy_supportsfeeds', '0/feed');
variable_set('pathauto_transliterate', false);
variable_set('pathauto_update_action', '1');
variable_set('pathauto_user_bulkupdate', 0);
variable_set('pathauto_user_pattern', 'users/[user-raw]');
variable_set('pathauto_user_supportsfeeds', NULL);
variable_set('pathauto_verbose', 0);
variable_set('service_links_category_types', array (
));
variable_set('service_links_in_links', '0');
variable_set('service_links_in_node', '2');
variable_set('service_links_node_types', array (
  'event' => 'event',
  'news' => 'news',
  'sermon' => 'sermon',
  'service' => 'service',
  'committee' => 0,
  'group' => 0,
  'link' => 0,
  'newsletter' => 0,
  'page' => 0,
  'profile' => 0,
  'reclass' => 0,
  'webform' => 0,
));
variable_set('service_links_show_delicious', 1);
variable_set('service_links_show_digg', 1);
variable_set('service_links_show_facebook', 1);
variable_set('service_links_show_furl', 0);
variable_set('service_links_show_google', 0);
variable_set('service_links_show_icerocket', 0);
variable_set('service_links_show_magnoliacom', 0);
variable_set('service_links_show_newsvine', 0);
variable_set('service_links_show_propeller', 0);
variable_set('service_links_show_reddit', 0);
variable_set('service_links_show_stumbleupon', 1);
variable_set('service_links_show_technorati', 0);
variable_set('service_links_show_yahoo', 0);
variable_set('service_links_style', '2');
variable_set('site_frontpage', 'home');
variable_set('site_mission', '');
variable_set('theme_default', 'garland');
variable_set('theme_settings', array (
  'toggle_logo' => 1,
  'toggle_name' => 1,
  'toggle_slogan' => 0,
  'toggle_mission' => 1,
  'toggle_node_user_picture' => 0,
  'toggle_comment_user_picture' => 0,
  'toggle_search' => 0,
  'toggle_favicon' => 0,
  'toggle_primary_links' => 1,
  'toggle_secondary_links' => 1,
  'toggle_node_info_blog' => 1,
  'toggle_node_info_committee' => 0,
  'toggle_node_info_event' => 0,
  'toggle_node_info_group' => 0,
  'toggle_node_info_link' => 0,
  'toggle_node_info_news' => 0,
  'toggle_node_info_newsletter' => 0,
  'toggle_node_info_page' => 0,
  'toggle_node_info_profile' => 0,
  'toggle_node_info_reclass' => 0,
  'toggle_node_info_sermon' => 0,
  'toggle_node_info_service' => 0,
  'toggle_node_info_webform' => 0,
  'default_logo' => 1,
  'logo_path' => '',
  'logo_upload' => '',
  'default_favicon' => 1,
  'favicon_path' => '',
  'favicon_upload' => '',
  'nice_menus_custom_css' => '',
));
variable_set('user_email_verification', 1);
variable_set('user_mail_password_reset_body', '!username,

A request to reset the password for your account has been made at !site.

You may now log in to !uri_brief by clicking on this link or copying and pasting it in your browser:

!login_url

This is a one-time login, so it can be used only once. It expires after one day and nothing will happen if it\'s not used.

After logging in, you will be redirected to !edit_uri so you can change your password.');
variable_set('user_mail_password_reset_subject', 'Replacement login information for !username at !site');
variable_set('user_mail_register_admin_created_body', '!username,

A site administrator at !site has created an account for you. You may now log in to !login_uri using the following username and password:

username: !username
password: !password

You may also log in by clicking on this link or copying and pasting it in your browser:

!login_url

This is a one-time login, so it can be used only once.

After logging in, you will be redirected to !edit_uri so you can change your password.


--  !site team');
variable_set('user_mail_register_admin_created_subject', 'An administrator created an account for you at !site');
variable_set('user_mail_register_no_approval_required_body', '!username,

Thank you for registering at !site. You may now log in to !login_uri using the following username and password:

username: !username
password: !password

You may also log in by clicking on this link or copying and pasting it in your browser:

!login_url

This is a one-time login, so it can be used only once.

After logging in, you will be redirected to !edit_uri so you can change your password.


--  !site team');
variable_set('user_mail_register_no_approval_required_subject', 'Account details for !username at !site');
variable_set('user_mail_register_pending_approval_body', '!username,

Thank you for registering at !site. Your application for an account is currently pending approval. Once it has been approved, you will receive another e-mail containing information about how to log in, set your password, and other details.


--  !site team');
variable_set('user_mail_register_pending_approval_subject', 'Account details for !username at !site (pending admin approval)');
variable_set('user_mail_status_activated_body', '!username,

Your account at !site has been activated.

You may now log in by clicking on this link or copying and pasting it in your browser:

!login_url

This is a one-time login, so it can be used only once.

After logging in, you will be redirected to !edit_uri so you can change your password.

Once you have set your own password, you will be able to log in to !login_uri in the future using:

username: !username
');
variable_set('user_mail_status_activated_notify', 1);
variable_set('user_mail_status_activated_subject', 'Account details for !username at !site (approved)');
variable_set('user_mail_status_blocked_body', '!username,

Your account on !site has been blocked.');
variable_set('user_mail_status_blocked_notify', 0);
variable_set('user_mail_status_blocked_subject', 'Account details for !username at !site (blocked)');
variable_set('user_mail_status_deleted_body', '!username,

Your account on !site has been deleted.');
variable_set('user_mail_status_deleted_notify', 0);
variable_set('user_mail_status_deleted_subject', 'Account details for !username at !site (deleted)');
variable_set('user_pictures', '1');
variable_set('user_picture_default', '');
variable_set('user_picture_dimensions', '150x150');
variable_set('user_picture_file_size', '200');
variable_set('user_picture_guidelines', '');
variable_set('user_picture_path', 'pictures');
variable_set('user_register', '0');
variable_set('user_registration_help', '');
variable_set('user_signatures', '0');
variable_set('views_block_hashes', array (
));
variable_set('views_defaults', array (
  'calendar' => false,
));
variable_set('webform_menu_settings', array (
  1 => 1,
));
