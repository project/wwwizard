<?php

/**
 * @file
 * The Welcoming Websites Wizard installation profile
 */

/**
 * Implementation of hook_profile_modules.
 */
function wwwizard_profile_details() {
  return array(
    'name' => st('Welcoming Websites Wizard'),
    'description' => st('An installation profile for building websites for Unitarian Universalist congregations.'),
  );
}

/**
 * Implementation of hook_profile_modules.
 */
function wwwizard_profile_modules() {
  $modules = array(
    // required core modules
    'block', 'filter', 'node', 'system', 'user', 

    // optional core modules
    'color', 'dblog', 'help', 'menu', 'path', 'taxonomy', 'update', 'blog',

    // installed modules
    // admin
    'advanced_help', 'admin_menu', 'adminrole', 'ezmenu', 'ctm',

    // cck
    'token', 'content', 'email', 'fieldgroup', 'filefield', 'getid3', 'filefield_meta', 'link', 'number', 'optionwidgets', 'text', 'nodereference', 'userreference', 'nodereference_url',

    // content profile
    'content_profile', 'content_profile_registration', 'wwwizard_profile',

    // views
    'views', 'views_export', 'views_ui',

    // date/time
    'date_api', 'date_timezone', 'date', 'date_popup', 'date_repeat', 'calendar', 'calendar_ical', 'jcalendar', 

    // input filters
    'spamspan',

    // other
    'admin_links', 'auto_nodetitle', 'globalredirect', 'imce', 'nice_menus', 'pathauto', 'webform', 'addresses', 'addresses_cck', 'addresses_phone',

    // user interface
    'wysiwyg', 'reg_with_pic', 'wwwizard_admin', 'service_links', 'better_formats', 'imce_wysiwyg', 'wwwizard_display',

    // needed for install profile
    'content_copy', 'wwwizard_views', 'install_profile_api'
  );
  
  return $modules;
}

/**
 * Implementation of hook_profile_task_list.
 */
function wwwizard_profile_task_list() {
  // create editor user
}

function wwwizard_profile_tasks(&$task, $url) {
  install_include(wwwizard_profile_modules());
  
  $rids = wwwizard_profile_create_roles_and_perms(); // do this first so we can reference ministers in sermon content type
  
  include_once(drupal_get_path('module', 'install_profile_api') . '/contrib/content_copy.inc');
  wwwizard_profile_create_content_types();
  
  wwwizard_profile_update_perms($rids['editor']); // now give admin and editor content type permis
  
  foreach (array('vocabs', 'menus', 'settings') as $key) {
    wwwizard_profile_create_from_file($key);  
  }
  
  wwwizard_profile_setup_blocks();
  
  wwwizard_profile_setup_filters($rids);
  
  wwwizard_profile_setup_wysiswyg($rids);

  wwwizard_profile_setup_dates();
  
  wwwizard_profile_create_home_page();
  
  // Update the menu router information.
  menu_rebuild();
  
  // allow our modules to have the last say on forms, theme preprocessing, etc
  db_query("update {system} set weight = 10 where name like 'wwwizard_%%'");
}

/**
 * Create roles and permissions.
 */
function wwwizard_profile_create_roles_and_perms() {
  // anonymous and authenticated get access content; that's it!
  install_add_permissions(1, array('access content'));
  install_add_permissions(2, array('access content'));
  
  $roles = array(
    'administrator',
    'editor',
    'minister',
    'staff',
  );
  
  foreach ($roles as $role) {
    $rids[$role] = install_add_role($role);
  }
  
  // give administrator all permissions
  variable_set('adminrole_adminrole', $administrator_rid);
  adminrole_update_perms(); // TODO: does this work?
  
  // now for the editor
  $permissions = array(
    "administer blocks",
    "administer menu",
    "administer nodes",
    "delete revisions",
    "revert revisions",
    "view revisions",
    "select different theme",
    "access user profiles",
    "administer permissions",
    "administer users",
    "access webform results",
    "clear webform results",
    "create webforms",
    "edit own webform submissions",
    "edit own webforms",
    "edit webform submissions",
    "edit webforms",
    "create blog entries",
    "delete any blog entry",
    "delete own blog entries",
    "edit any blog entry",
    "edit own blog entries",
  );
  install_add_permissions($rids['editor'], $permissions);
  
  // let the minister blog
  $permissions = array(
    "create blog entries",
    "delete own blog entries",
    "edit own blog entries",
  );  
  install_add_permissions($rids['minister'], $permissions);
  
  return $rids;
}

/**
 * Give the administrator and editor roles all content type permissions
 */
function wwwizard_profile_update_perms($editor_rid) {
  adminrole_update_perms();
  
  $permission = array();
  foreach (node_get_types() as $type => $info) {
    if ($type == 'webform') { continue; }
    
    $permissions[] = "create $type content";
    $permissions[] = "edit any $type content";
    $permissions[] = "delete any $type content";
    $permissions[] = "edit own $type content";
    $permissions[] = "delete own $type content";
  }
  
  foreach ($permissions as $perm) {
    install_add_permissions($editor_rid, $permissions);
  }
}

function wwwizard_profile_create_content_types() {
  $path = drupal_get_path('profile', 'wwwizard') .'/content_types';
  
  $res = opendir($path);
  if (!$res) { return ; }
  
  while ($file = readdir($res)) {
    $matches = array();
    if (preg_match('~^(.*)\.inc$~', $file, $matches)) {
      $file_path = "$path/$file";
      
      $type_name = trim($matches[1]);
      
      if (!in_array($type_name, array('webform', 'profile', 'blog'))) { // make way for module-created types
        $type_name = '';
      }
      
      install_content_copy_import_from_file($file_path, $type_name);
    }
  }

  if (!file_exists(file_directory_path() . '/pictures')) {
    mkdir(file_directory_path() . '/pictures', 0755);
  }
}

function wwwizard_profile_create_from_file($file) {
  include_once(drupal_get_path('profile', 'wwwizard') ."/$file.inc");
}

function wwwizard_profile_setup_blocks() {
  db_query("update {blocks} set status = 0 where module='user' and delta=1 and theme='garland'");
  db_query("update {blocks} set weight = -9 where module='user' and delta=0 and theme='garland'");
  install_add_block('nice_menus', 1, 'garland', 1, -10, 'left');
  db_query("update {blocks} set title='<none>' where module='nice_menus' and delta=1 and theme='garland'");
  install_add_block('menu', 'menu-site-manage', 'garland', 1, -8, 'left');
  install_add_block('wwwizard_display', 'poweredby', 'garland', 1, 10, 'footer');
}

function wwwizard_profile_setup_filters($rids) {
  db_query("update {filters} set weight=-10 where format=2 and module='filter' and delta=3");
  db_query("insert into {filters} (format, module, delta, weight) values (2, 'spamspan', 0, -9)");
  db_query("update {filters} set weight=-10 where format=2 and module='filter' and delta=2");
  db_query("delete from {filters} where format=2 and module='filter' and delta=2");
  
  db_query("update {filter_formats} set roles='%d,%d' where format=2", $rids['administrator'], $rids['editor']);
}

function wwwizard_profile_setup_wysiswyg($rids) {
  $settings = array (
    'default' => 1,
    'user_choose' => 0,
    'show_toggle' => 1,
    'theme' => 'advanced',
    'language' => 'en',
    'buttons' => array (
      'default' => array (
        'bold' => 1,
        'italic' => 1,
        'underline' => 1,
        'strikethrough' => 1,
        'justifyleft' => 1,
        'justifycenter' => 1,
        'justifyright' => 1,
        'justifyfull' => 1,
        'bullist' => 1,
        'numlist' => 1,
        'outdent' => 1,
        'indent' => 1,
        'undo' => 1,
        'redo' => 1,
        'link' => 1,
        'unlink' => 1,
        'anchor' => 1,
        'image' => 1,
        'cleanup' => 1,
        'sup' => 1,
        'sub' => 1,
        'blockquote' => 1,
        'code' => 1,
        'cut' => 1,
        'copy' => 1,
        'paste' => 1,
      ),
      'imce' => array (
        'imce' => 1,
      ),
      'wysiwyg' => array (
        'break' => 1,
      ),
    ),
    'toolbar_loc' => 'top',
    'toolbar_align' => 'left',
    'path_loc' => 'bottom',
    'resizing' => 1,
    'verify_html' => 1,
    'preformatted' => 0,
    'convert_fonts_to_spans' => 1,
    'remove_linebreaks' => 0,
    'apply_source_formatting' => 0,
    'paste_auto_cleanup_on_paste' => 1,
    'block_formats' => 'p,address,pre,h2,h3,h4,h5,h6,div',
    'css_setting' => 'none',
    'css_path' => '',
    'css_classes' => '',
  );
  
  $settings = serialize($settings);
  
  if (db_result(db_query("select count(*) as num from {wysiwyg} where format=2"))) {
    db_query("update {wysiwyg} set editor='tinymce', settings='%s where format=2", $settings);
  }
  else {
    db_query("insert into {wysiwyg} (format, editor, settings) values(2, 'tinymce', '%s')", $settings);
  }
  
  variable_set('imce_roles_profiles', array (
    $rids['editor'] => array (
      'weight' => '0',
      'pid' => '2',
    ),
    $rids['administrator'] => array (
      'weight' => '0',
      'pid' => '2',
    ),
    2 => array (
      'weight' => 11,
      'pid' => '0',
    ),
    1 => array (
      'weight' => 12,
      'pid' => '0',
    ),
  ));
  
  foreach ($rids as $role => $rid) {
    foreach (array('node', 'comment') as $type) {
      $format = in_array($role, array('administrator', 'editor')) ? 2 : 1; // full html for admin and editor only
      db_query("UPDATE {better_formats_defaults} SET format=%d, weight=%d WHERE rid=%d AND type='%s'", $format, 0, $rid, $type);
    }  
  }
}

function wwwizard_profile_setup_dates() {
  // create "short time" format
  $format = array();
  $format['format'] = "g:i a";
  $format['type'] = 'custom';
  $format['locked'] = 0;
  $format['is_new'] = 1;
  date_format_save($format);
  
  $format_type = array(
    'title' => 'Short time',
    'type' => 'short_time',
    'locked' => 0,
    'is_new' => 1,
  );
  date_format_type_save($format_type);
  variable_set("date_format_short_time", "g:i a");
}

function wwwizard_profile_create_home_page() {
  $node = new stdClass();
  $node->type = 'page';
  $node->uid = 1;
  $node->title = st('Home');
  $node->body = '<p>'
    . st('Congratulations!  You are ready to begin work on your new Welcoming Websites Wizard-powered site.') . "</p>\n<p>"
    . st('We recommend that you read our !start page for a list of tasks to get you familiar with all of the features available in the Wizard.', array('!start' => l(st('Getting started'), 'help/wwwizard_admin/getting-started'))) . "</p>\n<p>"
    . st('When you are ready to launch your new site, don\'t forget to !edit and welcome visitors to your site.', array('!edit' => l(st('edit this page'), 'node/1/edit'))) . "</p>\n";
  $node->path = 'home';
  $node = node_submit($node);
  node_save($node);
  variable_set('wwwizard_homepage_nid', $node->nid);
}
