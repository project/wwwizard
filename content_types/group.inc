<?php
$content[type]  = array (
  'name' => 'Group',
  'type' => 'group',
  'description' => 'A group of congregants not involved in governance, such as a social justice group, a book club, or a spiritual practice / prayer group.',
  'title_label' => 'Name',
  'body_label' => 'Description',
  'min_word_count' => '0',
  'help' => '',
  'node_options' => 
  array (
    'status' => true,
    'promote' => false,
    'sticky' => false,
    'revision' => false,
  ),
  'old_type' => 'group',
  'orig_type' => '',
  'module' => 'node',
  'custom' => '1',
  'modified' => '1',
  'locked' => '0',
  'content_profile_use' => 0,
  'menu' => 
  array (
    'menu-site-manage' => false,
    'navigation' => false,
    'primary-links' => false,
    'secondary-links' => false,
    'menu-sitemap' => false,
  ),
  'ant' => '0',
  'ant_pattern' => '',
  'ant_php' => 0,
);
