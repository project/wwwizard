<?php
$content[type]  = array (
  'name' => 'Page',
  'type' => 'page',
  'description' => 'A <em>page</em> is a simple method for creating and displaying information that rarely changes, such as an "About us" section of a website.',
  'title_label' => 'Title',
  'body_label' => 'Body',
  'min_word_count' => '0',
  'help' => '',
  'node_options' => 
  array (
    'status' => true,
    'promote' => false,
    'sticky' => false,
    'revision' => false,
  ),
  'old_type' => 'page',
  'orig_type' => 'page',
  'module' => 'node',
  'custom' => '1',
  'modified' => '1',
  'locked' => '0',
  'content_profile_use' => 0,
  'menu' => 
  array (
    'menu-site-manage' => false,
    'navigation' => false,
    'primary-links' => false,
    'secondary-links' => false,
    'menu-sitemap' => false,
  ),
  'ant' => '0',
  'ant_pattern' => '',
  'ant_php' => 0,
);
