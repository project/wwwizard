<?php
$content[type]  = array (
  'name' => 'Sermon',
  'type' => 'sermon',
  'description' => 'A sermon given at a weekly service.',
  'title_label' => 'Title',
  'body_label' => 'Text',
  'min_word_count' => '0',
  'help' => '',
  'node_options' => 
  array (
    'status' => true,
    'promote' => false,
    'sticky' => false,
    'revision' => false,
  ),
  'old_type' => 'sermon',
  'orig_type' => '',
  'module' => 'node',
  'custom' => '1',
  'modified' => '1',
  'locked' => '0',
  'content_profile_use' => 0,
  'menu' => 
  array (
    'menu-site-manage' => false,
    'navigation' => false,
    'primary-links' => false,
    'secondary-links' => false,
    'menu-sitemap' => false,
  ),
  'ant' => '0',
  'ant_pattern' => '',
  'ant_php' => 0,
);
$content[fields]  = array (
  0 => 
  array (
    'label' => 'Service',
    'field_name' => 'field_service',
    'type' => 'nodereference',
    'widget_type' => 'nodereference_url',
    'change' => 'Change basic information',
    'weight' => '-4',
    'fallback' => 'select',
    'node_link' => 
    array (
      'enabled' => 1,
      'title' => 'Add a sermon',
      'hover_title' => '',
    ),
    'description' => '',
    'group' => false,
    'required' => 1,
    'multiple' => '0',
    'referenceable_types' => 
    array (
      'service' => 'service',
      'committee' => 0,
      'event' => 0,
      'group' => 0,
      'link' => 0,
      'news' => 0,
      'newsletter' => 0,
      'page' => 0,
      'profile' => 0,
      'reclass' => 0,
      'sermon' => 0,
      'webform' => 0,
      'blog' => false,
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'op' => 'Save field settings',
    'module' => 'nodereference',
    'widget_module' => 'nodereference_url',
    'columns' => 
    array (
      'nid' => 
      array (
        'type' => 'int',
        'unsigned' => true,
        'not null' => false,
      ),
    ),
    'display_settings' => 
    array (
      'label' => 
      array (
        'format' => 'inline',
        'exclude' => 0,
      ),
      'teaser' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      4 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  ),
  1 => 
  array (
    'label' => 'Preacher',
    'field_name' => 'field_preacher',
    'type' => 'userreference',
    'widget_type' => 'userreference_select',
    'change' => 'Change basic information',
    'weight' => '-3',
    'autocomplete_match' => 'contains',
    'reverse_link' => 1,
    'description' => '',
    'default_value' => 
    array (
      0 => 
      array (
        'uid' => '',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' => 
    array (
      'field_preacher' => 
      array (
        0 => 
        array (
          'uid' => '',
        ),
        'uid' => 
        array (
          'uid' => '',
        ),
      ),
    ),
    'group' => false,
    'required' => 0,
    'multiple' => '0',
    'referenceable_roles' => 
    array (
      5 => 5,
      2 => 0,
      3 => 0,
      4 => 0,
      7 => 0,
      6 => 0,
    ),
    'referenceable_status' => 
    array (
      1 => 1,
      0 => 0,
    ),
    'op' => 'Save field settings',
    'module' => 'userreference',
    'widget_module' => 'userreference',
    'columns' => 
    array (
      'uid' => 
      array (
        'type' => 'int',
        'unsigned' => true,
        'not null' => false,
      ),
    ),
    'display_settings' => 
    array (
      'weight' => '7',
      'parent' => 'group_sermon',
      'label' => 
      array (
        'format' => 'inline',
      ),
      'teaser' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      4 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  ),
  2 => 
  array (
    'label' => 'Preacher (other)',
    'field_name' => 'field_preacher_other',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'change' => 'Change basic information',
    'weight' => '-2',
    'rows' => 5,
    'size' => '60',
    'description' => '',
    'default_value' => 
    array (
      0 => 
      array (
        'value' => '',
        '_error_element' => 'default_value_widget][field_preacher_other][0][value',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' => 
    array (
      'field_preacher_other' => 
      array (
        0 => 
        array (
          'value' => '',
          '_error_element' => 'default_value_widget][field_preacher_other][0][value',
        ),
      ),
    ),
    'group' => false,
    'required' => 0,
    'multiple' => '0',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'op' => 'Save field settings',
    'module' => 'text',
    'widget_module' => 'text',
    'columns' => 
    array (
      'value' => 
      array (
        'type' => 'text',
        'size' => 'big',
        'not null' => false,
        'sortable' => true,
      ),
    ),
    'display_settings' => 
    array (
      'weight' => '8',
      'parent' => 'group_sermon',
      'label' => 
      array (
        'format' => 'inline',
      ),
      'teaser' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      4 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  ),
);
$content[extra]  = array (
  'title' => '-5',
  'body_field' => '0',
  'menu' => '-1',
);
