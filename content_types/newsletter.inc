<?php
$content[type]  = array (
  'name' => 'Newsletter',
  'type' => 'newsletter',
  'description' => 'A periodic publication with important information about congregational life, upcoming events, and messages from ministerial staff.',
  'title_label' => 'Title',
  'body_label' => 'Description or contents',
  'min_word_count' => '0',
  'help' => 'The title field is optional.  If it is left blank, the system will automatically generate a title based on the publication date, e.g. "Newsletter for January 1, 2000".',
  'node_options' => 
  array (
    'status' => true,
    'promote' => false,
    'sticky' => false,
    'revision' => false,
  ),
  'old_type' => 'newsletter',
  'orig_type' => '',
  'module' => 'node',
  'custom' => '1',
  'modified' => '1',
  'locked' => '0',
  'content_profile_use' => 0,
  'menu' => 
  array (
    'menu-site-manage' => false,
    'navigation' => false,
    'primary-links' => false,
    'secondary-links' => false,
    'menu-sitemap' => false,
  ),
  'ant' => '2',
  'ant_pattern' => 'Newsletter for [field_pubdate-month] [field_pubdate-d], [field_pubdate-yyyy]',
  'ant_php' => 0,
);
$content[fields]  = array (
  0 => 
  array (
    'label' => 'Publication date',
    'field_name' => 'field_pubdate',
    'type' => 'date',
    'widget_type' => 'date_popup',
    'change' => 'Change basic information',
    'weight' => '-4',
    'default_value' => 'now',
    'default_value_code' => '',
    'default_value2' => 'same',
    'default_value_code2' => '',
    'input_format' => 'M j Y',
    'input_format_custom' => '',
    'year_range' => '-3:+3',
    'increment' => '1',
    'advanced' => 
    array (
      'label_position' => 'above',
      'text_parts' => 
      array (
        'year' => 0,
        'month' => 0,
        'day' => 0,
        'hour' => 0,
        'minute' => 0,
        'second' => 0,
      ),
    ),
    'label_position' => 'above',
    'text_parts' => 
    array (
    ),
    'description' => '',
    'group' => false,
    'required' => 0,
    'multiple' => '0',
    'repeat' => 0,
    'todate' => '',
    'granularity' => 
    array (
      'year' => 'year',
      'month' => 'month',
      'day' => 'day',
    ),
    'output_format_date' => 'F j, Y',
    'output_format_custom' => '',
    'output_format_date_long' => 'm/d/Y',
    'output_format_custom_long' => '',
    'output_format_date_medium' => 'm/d/Y',
    'output_format_custom_medium' => '',
    'output_format_date_short' => 'm/d/Y',
    'output_format_custom_short' => '',
    'tz_handling' => 'none',
    'timezone_db' => 'UTC',
    'op' => 'Save field settings',
    'module' => 'date',
    'widget_module' => 'date',
    'columns' => 
    array (
      'value' => 
      array (
        'type' => 'varchar',
        'length' => 20,
        'not null' => false,
        'sortable' => true,
      ),
    ),
    'display_settings' => 
    array (
      'weight' => '-4',
      'parent' => '',
      'label' => 
      array (
        'format' => 'inline',
      ),
      'teaser' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      4 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  ),
  1 => 
  array (
    'label' => 'Newsletter file',
    'field_name' => 'field_newsletter_file',
    'type' => 'filefield',
    'widget_type' => 'filefield_widget',
    'change' => 'Change basic information',
    'weight' => '-3',
    'file_extensions' => 'txt pdf doc',
    'file_path' => 'newsletters',
    'max_filesize_per_file' => '',
    'max_filesize_per_node' => '',
    'description' => '',
    'group' => false,
    'required' => 0,
    'multiple' => '0',
    'list_default' => '1',
    'force_list_default' => '1',
    'show_description' => '0',
    'op' => 'Save field settings',
    'module' => 'filefield',
    'widget_module' => 'filefield',
    'columns' => 
    array (
      'fid' => 
      array (
        'type' => 'int',
        'not null' => false,
      ),
      'list' => 
      array (
        'type' => 'int',
        'size' => 'tiny',
        'not null' => false,
      ),
      'data' => 
      array (
        'type' => 'text',
        'serialize' => true,
      ),
    ),
    'display_settings' => 
    array (
      'weight' => '-3',
      'parent' => '',
      'label' => 
      array (
        'format' => 'hidden',
      ),
      'teaser' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      4 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  ),
);
$content[extra]  = array (
  'title' => '-5',
  'body_field' => '-1',
  'menu' => '-2',
);
