<?php
$content[type]  = array (
  'name' => 'Blog entry',
  'type' => 'blog',
  'description' => 'A <em>blog entry</em> is a single post to an online journal, or <em>blog</em>.',
  'title_label' => 'Title',
  'body_label' => 'Body',
  'min_word_count' => '0',
  'help' => '',
  'node_options' => 
  array (
    'status' => true,
    'promote' => false,
    'sticky' => false,
    'revision' => false,
  ),
  'old_type' => 'blog',
  'orig_type' => 'blog',
  'module' => 'blog',
  'custom' => '0',
  'modified' => '1',
  'locked' => '1',
  'reset' => 'Reset to defaults',
  'content_profile_use' => 0,
  'menu' => 
  array (
    'menu-site-manage' => false,
    'navigation' => false,
    'primary-links' => false,
    'secondary-links' => false,
    'menu-sitemap' => false,
  ),
  'ant' => '0',
  'ant_pattern' => '',
  'ant_php' => 0,
);
